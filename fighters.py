from abc import abstractmethod
from abc import ABCMeta
#Clase abstracta para las clases warrior y wizards
class Fighter(metaclass = ABCMeta):
	@abstractmethod
	def isVulnerable(self):
		pass
	
	@abstractmethod
	def perdervida(self, damage):
		pass
	
