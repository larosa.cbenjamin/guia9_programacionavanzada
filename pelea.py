from warrior import Warrior
from wizards import Wizard	

"""En este problema no se aplico herencia debido a las diferencias
dentro de el metodo isVulnerable y el tipo del peleador, un extra que podria
tener el codigo para añadir herencia podria ser añadir armas a cada una de las
clases"""


class Fight():
	def __init__(self, fighter_one, fighter_two):
		self._fighter_one = fighter_one
		self._fighter_two = fighter_two
	
	@property
	def fighter_one(self):
		return self._fighter_one
		
	
	@fighter_one.setter
	def fighter_one(self, fighter_one):
		if isinstance (fighter_one, Warrior) or isinstance (fighter_one, Wizard):
			self._fighter_one = fighter_one
		else:
			print("El dato ingresado no es valido")
	
	@property
	def fighter_two(self):
		return self._fighter_two
		
	@fighter_two.setter
	def fighter_two(self, fighter_two):
		if isinstance (fighter_two, Warrior) or isinstance (fighter_two, Wizard):
			self._fighter_two = fighter_two
		else:
			print("El dato ingresado no es valido")
	
	def damagepoints(self, afectado):
		if afectado == self._fighter_one:
			if isinstance(self._fighter_two, Warrior):
				vulnerable = self._fighter_two.isVulnerable()
				if vulnerable == True:
					afectado.perdervida(6)
				else:
					afectado.perdervida(10)
			if isinstance(self._fighter_two, Wizard):
				vulnerable = self._fighter_two.isVulnerable()
				if vulnerable == True:
					afectado.perdervida(12)
				else:
					afectado.perdervida(3)
		if afectado == self._fighter_two:
			if isinstance(self._fighter_one, Warrior):
				vulnerable = self._fighter_one.isVulnerable()
				if vulnerable == True:
					afectado.perdervida(6)
				else:
					afectado.perdervida(10)
			if isinstance(self._fighter_one, Wizard):
				vulnerable = self._fighter_one.isVulnerable()
				if vulnerable == True:
					afectado.perdervida(12)
				else:
					afectado.perdervida(3)

rocky = Warrior("Rocky")
lucas = Wizard("Lucas")
print(rocky.tipo)
print(lucas.tipo)
round_one = Fight(lucas, rocky)
round_one.damagepoints(rocky)
lucas.preparespell()
round_one.damagepoints(rocky)
round_one.damagepoints(lucas)


	
	
