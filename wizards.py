from fighters import Fighter
class Wizard(Fighter):
	def __init__(self, nombre):
		self._tipo = "<MAGE_TYPE>"
		self._nombre = nombre
		self._health = 50
		self._cargahechizo = 0
	
	@property
	def tipo(self):
		return self._tipo
		
	@property
	def nombre(self):
		return self._nombre
	
	@nombre.setter
	def nombre(self, nombre):
		if isintance (nombre, str):
			self._nombre = nombre
		else:
			print("El dato ingresado no es valido")
	
	def preparespell(self):
		if self._cargahechizo == 0:
			print(self._nombre,"Ha preparado su hechizo")
			self._cargahechizo = 1
		else:
			print(self._nombre,"Ya tiene cargado el hechizo")
	
	def isVulnerable(self):
		if self._cargahechizo == 0:
			return False
		else:
			return True
	
	def perdervida(self, damage):
		self._health = self._health - damage
		print(self._nombre, "Ha perdido", damage)
	
		
	
